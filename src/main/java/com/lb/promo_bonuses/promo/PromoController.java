package com.lb.promo_bonuses.promo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/promo")
public class PromoController {
    private final PromoRepository promoRepository;

    public PromoController(PromoRepository promoRepository) {
        this.promoRepository = promoRepository;
    }

    @GetMapping
    public @ResponseBody
    Iterable<Promo> loadPromos() {
        return promoRepository.findAll();
    }

}
