package com.lb.promo_bonuses.promo;

import org.springframework.data.repository.CrudRepository;

public interface PromoRepository extends CrudRepository<Promo, Long> {

}
