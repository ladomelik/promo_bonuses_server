package com.lb.promo_bonuses.game;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GameRepository extends CrudRepository<Game, Long> {

    @Query("select g from Game g where g.deleted = false")
    List<Game> loadActiveGames();
}
