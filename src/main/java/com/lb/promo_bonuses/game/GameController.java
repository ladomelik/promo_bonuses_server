package com.lb.promo_bonuses.game;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/game")
public class GameController {
    private final GameRepository gameRepository;

    public GameController(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @GetMapping
    public @ResponseBody
    List<Game> loadGames() {
        return gameRepository.loadActiveGames();
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody
    Game getGame(@PathVariable long id) {
        return gameRepository.findById(id).orElse(null);
    }

    @PostMapping
    public @ResponseBody
    Game createGame(@RequestBody Game game) {
        return gameRepository.save(game);
    }

    @PutMapping(path = "/{id}")
    public @ResponseBody
    Game updateGame(@PathVariable long id, @RequestBody Game game) {
        deleteGame(id);
        return gameRepository.save(game);
    }

    @DeleteMapping(path = "/{id}")
    public @ResponseBody
    void deleteGame(@PathVariable long id) {
        Game oldGame = gameRepository.findById(id).orElse(null);
        if (oldGame != null) {
            oldGame.setDeleted(true);
            gameRepository.save(oldGame);
        }
    }

    @GetMapping(path = "/{id}/config")
    public @ResponseBody
    String getGameConfig(@PathVariable long id) {
        Game game = gameRepository.findById(id).orElse(null);
        if (game != null) {
            String config = game.getConfig();
            if (config != null && !config.isEmpty()) {
                return game.getConfig();
            }
            if (game.getProvider() != null && game.getProvider().getHasConfig()) {
                return game.getProvider().getDefaultConfig();
            }
        }
        return null;
    }

    @PutMapping(path = "/{id}/config")
    public @ResponseBody
    String updateGameConfig(@PathVariable long id, @RequestBody String config) {
        Game game = gameRepository.findById(id).orElse(null);
        if (game != null) {
            game.setConfig(config);
            gameRepository.save(game);
            return config;
        }
        return null;
    }
}
