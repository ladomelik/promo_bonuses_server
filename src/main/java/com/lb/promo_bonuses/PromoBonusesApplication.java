package com.lb.promo_bonuses;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PromoBonusesApplication {

    public static void main(String[] args) {
        SpringApplication.run(PromoBonusesApplication.class, args);
    }

}
