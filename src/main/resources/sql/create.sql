/*
SQLyog Ultimate v13.1.5  (64 bit)
MySQL - 8.0.16-commercial : Database - promo_bonuses
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE = ''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS */`promo_bonuses` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `promo_bonuses`;

/*Table structure for table `game_providers` */

DROP TABLE IF EXISTS `game_providers`;

CREATE TABLE `game_providers`
(
    `id`             int(11)      NOT NULL AUTO_INCREMENT,
    `type`           varchar(255) NOT NULL,
    `name`           varchar(255) NOT NULL,
    `operator_id`    int(11)               DEFAULT NULL,
    `has_config`     int(11)      NOT NULL DEFAULT '0',
    `default_config` text         NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 84
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

/*Data for the table `game_providers` */

insert into `game_providers`(`id`, `type`, `name`, `operator_id`, `has_config`, `default_config`)
values (10, 'TableGame', 'Table_all', NULL, 1, '{\r\n	\"gameID\":0\r\n}'),
       (11, 'TableGame', 'Domino', 524, 1, '{\r\n	\"gameID\":6\r\n}'),
       (12, 'TableGame', 'Bura', 521, 1, '{\r\n	\"gameID\":3\r\n}'),
       (13, 'TableGame', 'Seka', 522, 1, '{\r\n	\"gameID\":2\r\n}'),
       (14, 'TableGame', 'Nardi', 523, 1, '{\r\n	\"gameID\":1\r\n}'),
       (15, 'TableGame', 'Poker', 525, 1, '{\r\n	\"gameID\":4\r\n}'),
       (20, 'SpinGame', 'Spin_all', NULL, 1, '{\r\n	\"gameID\":501\r\n}'),
       (21, 'SpinGame', 'SpinDomino', 548, 1, '{\r\n	\"gameID\":548\r\n}'),
       (22, 'SpinGame', 'SpinBura', 545, 1, '{\r\n	\"gameID\":545\r\n}'),
       (23, 'SpinGame', 'SpinPoker', 541, 1, '{\r\n	\"gameID\":541\r\n}'),
       (24, 'SpinGame', 'SpinJoker', 534, 1, '{\r\n	\"gameID\":534\r\n}'),
       (30, 'Volt', 'Volt', 546, 1, '{\r\n	\"money\":-1,\r\n	\"coef\":-1\r\n}'),
       (40, 'FreeBet', 'Sport', 110, 0, '{}'),
       (58, 'Slots', 'PlayTech', 566, 1, '{\r\n	\"gameID\":-1\r\n}'),
       (59, 'Slots', 'NetEnt', 556, 1, '{\r\n	\"gameID\":-1,\r\n	\"betLevel\":-1,\r\n	\"coinLevel\":-1\r\n}'),
       (60, 'Slots', 'EGT', 508, 1, '{\r\n	\"campaign\":-1\r\n}'),
       (61, 'Slots', 'GameArt', 552, 1, '{\r\n	\"gameID\":-1,\r\n	\"betID\":-1\r\n}'),
       (62, 'Slots', 'PlaySon', 554, 1,
        '{\r\n	\"gameID\":-1,\r\n	\"lines\":-1,\r\n	\"coins\":-1,\r\n	\"bet-line\":-1\r\n}'),
       (63, 'Slots', 'PNG', 549, 1,
        '{\r\n	\"gameID\":-1,\r\n	\"line\":-1,\r\n	\"coins\":-1,\r\n	\"denomination\":-1\r\n}'),
       (64, 'Slots', 'Igrosoft', 544, 1,
        '{\r\n	\"gameID\":\"-1\",\r\n	\"denomination\":\"-1\",\r\n	\"betperline\":\"-1\",\r\n	\"lines\":\"-1\",\r\n	\"continue\":\"0\"\r\n}'),
       (65, 'Cash', 'Cash', NULL, 0, '{}'),
       (82, 'empty', 'empty', NULL, 0, '{}'),
       (83, 'Slots', 'Relax', 562, 1,
        '{\r\n    \"type\":\"featuretriggers | freespins\",\r\n    \"denomination\":-1,\r\n    \"gameid\":\'jamminjars\'\r\n}');

/*Table structure for table `games` */

DROP TABLE IF EXISTS `games`;

CREATE TABLE `games`
(
    `id`          int(11)      NOT NULL AUTO_INCREMENT,
    `promo_id`    int(11)      NOT NULL DEFAULT '1',
    `provider_id` int(11)      NOT NULL,
    `operator_id` int(11)      NOT NULL,
    `game_name`   varchar(255) NOT NULL,
    `config`      text,
    `description` varchar(255) NOT NULL,
    `url_desktop` varchar(255) NOT NULL,
    `url_mobile`  varchar(255) NOT NULL,
    `enabled`     int(11)               DEFAULT '1',
    `weight`      double                DEFAULT '1',
    `timestamp`   timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `deleted`     int(11)               DEFAULT '1',
    PRIMARY KEY (`id`),
    KEY `provider_id` (`provider_id`),
    CONSTRAINT `games_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `game_providers` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;


/*Data for the table `games` */

/*Table structure for table `promos` */

DROP TABLE IF EXISTS `promos`;

CREATE TABLE `promos`
(
    `id`         int(11)                                NOT NULL AUTO_INCREMENT,
    `name`       varchar(32) COLLATE utf8mb4_general_ci NOT NULL,
    `start_date` datetime                                        DEFAULT NULL,
    `end_date`   datetime                                        DEFAULT NULL,
    `timestamp`  timestamp                              NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `name` (`name`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

/*Data for the table `promos` */

insert into `promos`(`id`, `name`, `start_date`, `end_date`, `timestamp`)
values (1, 'golden_ball', '2021-04-21 00:00:00', '2021-05-01 00:00:00', '2021-04-18 16:26:04');

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;


alter table `games`
    add `game_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;
alter table `games`
    add `deleted` int(11) DEFAULT '1';

